"""
To render html web pages
"""
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.shortcuts import render, redirect

import datetime

from articles.models import Article


def home_view_old(request):
	"""
	take in a request(Django sends request)
	Return HTML as a response (We pick to retunr the response)
	"""
	if "accountLogout" in request.POST:
		return redirect('account_logout')

	articles_obj 	= Article.objects.get(id=1)
	articles_qs		= Article.objects.all() # QuerySet
	
	context = {
		"id"		:	articles_obj.id,
		"title" 	:	articles_obj.title,
		"content"	:	articles_obj.content,
		"obj_list"	:	articles_qs
	}
	
		#	H1_STRING = f"""
	#	<h1>{articles_obj.title}</h1> -> ({articles_obj.id})
	#	"""

	#	P_STRING = f"""
	#	<p>{articles_obj.content}</p>
	#	"""
		
	#	HTML_STRING = H1_STRING + P_STRING

	#	return HttpResponse(HTML_STRING)

	
	HTML_STRING = render_to_string('home-view.html', context=context)

	return HttpResponse(HTML_STRING)


def home_view_OLD(request, template):


	if "accountLogout" in request.POST:
		
		return redirect('account_logout')

	else:

		myDate 	= datetime.datetime.now()

		# articles_obj 	= Article.objects.get(id=1)
		# articles_qs		= Article.objects.all() # QuerySet
		
		articles_obj = None
		articles_qs = None
		
		context = {
			"id"		:	articles_obj.id,
			"title" 	:	articles_obj.title,
			"content"	:	articles_obj.content,
			"obj_list"	:	articles_qs,
			"myDate"	: 	myDate

		}

	return render(request, template, context)

def home_view(request, template, context=None):


	if "accountLogout" in request.POST:
		
		return redirect('account_logout')

	else:

		myDate 	= datetime.datetime.now()

		articles_qs		= Article.objects.all() # QuerySet
			
		if articles_qs:
			
			articles_obj 	= Article.objects.get(id=1)
			
			context = {
				"id"		:	articles_obj.id,
				"title" 	:	articles_obj.title,
				"content"	:	articles_obj.content,
				"obj_list"	:	articles_qs,
				"myDate"	: 	myDate

			}


	return render(request, template, context)