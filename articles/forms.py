from django import forms
from .models import Article


class ArticleForms(forms.ModelForm):
	title 		=  forms.CharField()
	content 	=  forms.CharField()


	class Meta:
			
		model = Article

		fields = ['title', 'content']

	