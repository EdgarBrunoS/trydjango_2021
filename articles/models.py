from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from django.urls import reverse
# Create your models here.


class Article(models.Model):
	title 		= models.CharField(max_length=50)
	slug 		= models.SlugField(max_length=50, blank=True, null=True)
	content 	= models.TextField()
	#publish_date = models.DateTimeField(default=timezone.now) 
	timestamp	 = models.DateTimeField(auto_now_add=True)
	updated      = models.DateTimeField(auto_now=True)
	publish		 = models.DateTimeField(auto_now=False, auto_now_add=False, default=timezone.now)

	def get_absolute_url(self):
		#return f'/articles/{self.slug}/'
		return reverse("article_view", kwargs={"slug" : self.slug})


	# Override Save Method
	def save(self, *args, **kwargs):
		# obj = Article.objects.get(id=1)
		super().save(*args, **kwargs)
