from django.db.models.signals import pre_save, post_save
from django.utils.text import slugify 
from django.dispatch import receiver
from .models import Article

 
def slugify_instance_title(instance, save=False):

	Klass = instance.__class__

	slug = slugify(f"{instance.title}-{instance.publish}")
	print(instance.id)

	# Reduce lenght slug
	if len(slug) > 50:
		sizeM = len(slug) - 50
		slug = slug[sizeM:]

	qs = Klass.objects.filter(slug=slug).exclude(id=instance.id)
	
	if qs.exists():
		slug = f"{slug}-{qs.count() +1}"
		# Reduce lenght slug
		if len(slug) > 50:
			return slugify_instance_title(instance, save)


	instance.slug = slug

	if save:
		instance.save()

	return instance


# Itself is going to happen before somethind is saved
@receiver(pre_save, sender=Article)
def article_pre_save(sender, instance, **kwargs):
	
	print("pre_save")
	
	if instance.slug is None:
		print('OK - PRE')
		slugify_instance_title(instance, save=False)
		
		
# Itself is going to happen after somethind is saved
@receiver(post_save, sender=Article)
def article_post_save(sender, instance, created, **kwargs):
	print("post_save")

	if created:
		print('OK - POST')
		slugify_instance_title(instance, save=True)