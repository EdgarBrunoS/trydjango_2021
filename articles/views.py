from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import Http404

from .forms import ArticleForms
from .models import Article

# Create your views here.


def article_search_view(request, template):
	
	query_dict = request.GET

	try:

		query = query_dict.get("query")
		article_obj = Article.objects.get(id=query)

	except:
		messages.info(request, f"Pesquisa de [{ query }] sem resultado válido")
		article_obj = {
			"title"		: "Item não encontrado!",
			"content"	: ''
		}

	context = {
		"obj"	: 	article_obj,
	}

	return render(request, template, context)

@login_required
def article_create_view(request, template):

	context = { 
		"form"	: ArticleForms()
	}

	if request.method == 'POST':

		form = ArticleForms(request.POST, request.FILES)

		if form.is_valid():
			instance = form.save(commit=False)
			messages.info(request, 'Three credits remain in your account.')
			messages.success(request, 'Profile details updated.')
			messages.warning(request, 'Your account expires in three days.')
			messages.error(request, 'Document deleted.')
			instance.save()
			
			return redirect('article_view',instance.id)
		else:

			context['form'] = form


	return render(request, template, context)

def article_home_view_OLD(request , template, slug=None):

		# article_obj = Article.objects.filter(slug=slug).exists()
		# Here, return a boolean value

	if slug:
		# send id URL BROWSER
		article_obj = Article.objects.get(slug=slug)
		
	else:

		article_obj = None

	context = {
		"obj"	: 	article_obj,
	}

	return render(request, template, context)
	# return redirect(article_obj.get_absoulte_url())


def article_home_view(request , template, slug=None):

	context = { }
	
	if slug:
		
		try:
			article_obj = Article.objects.get(slug=slug)
			context = { "obj"	: 	article_obj }
		
		except Article.DoesNotExist:
			raise Http404
		except:
			raise Http404

	return render(request, template, context)
