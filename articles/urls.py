from django.urls import path
from . import views

urlpatterns = [

	path('', views.article_search_view,		         {'template': 'article/search.html'}, name='article_search'),
    path('create/', views.article_create_view,       {'template': 'article/create.html'}, name='article_create'),
    path('<slug:slug>/', views.article_home_view,    {'template': 'article/detail.html'}, name='article_view')
    
 ]