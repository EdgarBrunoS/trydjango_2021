from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.shortcuts import render, redirect
from django.contrib import messages


# Create your views here.
def login_view(request, template):
	
	form = AuthenticationForm(request, data=request.POST)	

	context = {
		"action"	:	"account_login",
		"title"		:	"LOGIN",
		"btn"		:	"Login",
		"form"		:	form,
		"method"	:	"POST"
	}

	if request.user.is_authenticated:
		messages.info(request, f"You are already authenticate.")

		context = {

			"action"	:	"account_login",
			"title"		:	request.user,
			"btn"		:	"OK",
			"btn2"		:	"Logout",
			"method"	:	"GET",
			
		}

		if request.method == 'GET':
		
			if context['btn'] in request.GET:
				print("HOME !!!!!")
				return redirect('/')

			elif context['btn2'] in request.GET:
				print("LOGOUT !!!!")
				logout(request)
				return redirect('/')
			else:
				pass
				print("AQUI!!!!")


	elif request.method == 'POST':

		if form.is_valid():

			user = form.get_user()
			login(request, user)
			return redirect('account_login')

		else:
			messages.error(request, f'Invalid Username or Password.'),
	else:

		messages.warning(request, f'Insert your Username and Password to login.')


	return render(request, template, context)


def logout_view(request, template):

	context = {
			"action"	:	"account_login",
			"title"		:	request.user,
			"btn"		:	"OK",
			"method"	:	"GET"
	}

	print(template)
	print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

	if 'OK' in request.POST:
		print("HOME")
	elif 'Logout' in request.GET:
		print(LOGUT)
	else:
		print(request.GET)
		return render(request, template, context)