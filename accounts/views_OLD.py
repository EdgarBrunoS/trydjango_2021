from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.shortcuts import render, redirect
from django.contrib import messages


# Create your views here.
def login_view(request, template):

	if request.user.is_authenticated:
		
		messages.info(request, f"The user {request.user} is already authenticate.")
	
	elif request.method == 'POST':

		form = AuthenticationForm(request, data=request.POST)

		if form.is_valid():

			user = form.get_user()
			login(request, user)

			

		else:
			messages.error(request, f'Invalid username or passowrd.')
	else:
		print("NOOOOOOOOOOOOOOOOOOOO")






	context = {
		
		"action"	:	"account_login"
	}
	#messages.error(request, f'Insert your Username/e-mail and password to login')
	"""
	print('------------------------')
	print(request.POST.get("username"))
	print(request.POST.get("password"))
	print('------------------------')
	
	#form = authenticate(request, username=request.POST.get("Username"), password=request.POST.get("Password"))

	#form = authenticate(request.POST)

	
	context = {
		"action"	: "account_login"
	}

	if request.user.is_authenticated:

		context = {
			"message"	: f"The user {request.user} is already authenticate.",
			"action"	: "account_logout"
		}

	elif request.method == 'POST':

		form = AuthenticationForm(request, data=request.POST)	
		
		if form.is_valid():

			user = form.get_user()
			login(request, user)

			context = { 
				"message"	:	f"User {request.user} is authenticate successfully",
				"action"	:	"home_page"
			}

		else:

			context["error"] = "Invalid username or passowrd."
	else:

		context["message"] = "Insert your Username/e-mail and password to login."
	"""
	return render(request, template, context)


# OLD VERSION THAT VIEW
def login_view_OLD(request, template):

	if request.user.is_authenticated:

		context = {
			"message"	: f"The user {request.user} is already authenticate.",
			"action"	: "account_logout"
		}
	
	else:

		context = {
			"action"	: 'account_login'
		}

		if request.method == 'POST':
			
			username = request.POST.get("username")
			password = request.POST.get("password")
			
			# print(f"{username} - {password}") # REMOVE THIS !!!

			user = authenticate(request, username=username, password=password)

			if not user:

				context["error"] = "Invalid username or passowrd."

			else:

				login(request, user)
				context = { 
					"message"	:	f"User {request.user} authenticate successfully",
					"action"	:	"home_page"
				}

	return render(request, template, context)

def logout_view(request, template):
	
	context = {
		"message" 	: "Are you sure want to logout?",
		"action"	: "account_logout"
	}

	if "home_page" not in request.POST:

		if "account_logout" in request.POST:
			logout(request)
		else:
			return render(request, template, context)

	return redirect('/')

def register_view(request, template):

	form = UserCreationForm(request.POST or None)

	context = { "message"	:	"Register a new User!",
				"action"	:	"register_user",
				"form"		:	form,
				"name"		:	"OK"
	}
	
	if "OK" in request.POST:
	
		if form.is_valid():
			form.save()
			username = request.POST.get("username")
			context = { "message"	:	f"Wellcome new user {username} to the Site!",
						"action"	:	"account_login",
						"name"		:	None
			}

		else:
			context["error"] = "Somethyng is missing in your register."

	return render(request, template, context)
