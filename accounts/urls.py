from django.urls import path
from . import views

urlpatterns = [
	path('login',  views.login_view,       {'template' : 'accounts/login.html'},	name='account_login'),
	path('logout', views.login_view,       {'template' : 'accounts/login.html'},	name='account_logout'),
	#path('register', views.register_view, {'template' : 'accounts/register.html'},	name='register_user')
]